# Covid-19 Ontology on Test Information 


This project contains the Covid-19 Ontology on Test Information (COTI) and the documentations about the ontology.

The Covid-19 Ontology on Test Information (COTI) is developed in the Dutch uNLock (https://www.unlockapp.nl/) project 
to capture the different information elements that can be used to construct a Covid-19 test credential. In this Covid-19
 test credential you can identify data about yourself to prove, in this case, that you are recently tested negative for
  Covid-19. This can be used in multiple ways, such as when one wants to visit a nursing home. The person can then prove
   that they do not have Covid-19, and therefore are not a risk for the residents in the nursing home.
   
## Maintainers

Roos Bakker, TNO (roos.bakker@tno.nl)

Michiel Stornebrink, TNO (michiel.stornebrink@tno.nl)

## Repository

The ontology can be found in the folder [ontology](./ontology). An example for a Covid-19 test credential is shown in
[the example credential](./credentials/example_covid19_test_credential.json), under credentials. Lastly a more elaborate
documentation and a presentation describing and showing the ontology can be found under [documentation](./documentation).

## Main classes

The ontology is centered around two main classes: Covid-19 Test and Covid-19 Test Result. The Covid-19 Test class has 
4 data properties (the green labels) that can be used in a Covid-19 test credential: a test manufacturer, a test ID, 
a test name, and a test type. The Covid-19 Test Result class has the data property result, which can be positive or 
negative.

Furthermore, the Test class is connected to the Covid-19 Test Result class with two object properties: 'Covid-19 test 
result' and 'used test'. This translates to the fact that a Covid-19 Test has a Covid-19 Test Result, and the test 
result uses a test (to get to the result). 

Other important classes that have connections to data that can be used in a Covid-19 test credentials are Person, 
and Sample, and the PCR Test. Person has several data properties that can be used in a Covid-19 test credential: 
a family name, given names, and a birth date. It also has access through another class with Contact Details: a phone 
number, email address, and street address. The Sample class connects to sample data: A received date, a date on which
 the sample is taken, and the sample ID. Finally the PCR Test also has information like the specificity and the 
 sensitivity of the test.

The concepts and relations named above are the ones required to be able to form a Covid-19 test credential from them.

## Import classes

In this ontology several classes were used from other ontologies. One of those is the 
COviD-19 Ontology for Cases and Patient information (prefix: codo), by Biswanath, Dutta, and DeBellis, Michael, 
published by the Indian Statistical Institute (2020-04-27). From this ontology we used the data properties 
'hadCovidTest', 'resultDate', and the class 'UntestedForCovid'. The Friend of a Friend (prefix: foaf) ontology, 
by Brickley, Dan, and Miller, Libby, (2014) is used for the classes 'Person', and 'Organization'. These ontologies 
are licensed under the Creative Commons Attribution License (https://creativecommons.org/licenses/by/4.0/).

## Visualizing the ontology

A visualisation of the ontology gives a quick overview and can add to the understanding of the design choices 
described above. For displaying the ontology, first download the ontology to your device. Then go to the following link:

[webvowl semantic treehouse](https://webvowl.semantic-treehouse.nl/#file=COTIv1.1.owl)

Upload the ontology by going to the tab ***Ontology*** in the toolbar at the bottom of your screen. 
 Click ***select ontology file***, and select the COTI ontology.  



For editing the ontology for reuse we recommend using Protégé or Topbraid.

## Documentation

A more thorough description of the ontology (Annex 1), as well as more information about test credentials in general,
can be found in the following document:


[CCI-02: Covid-19 status](https://docs.google.com/document/d/1anv0qiqYJXypyprX7qbolVgJrA18QHZ8BQjBLBpIAcY/edit#heading=h.9315qp8zduc)


## Sources

#### Covid-19 experts: 
Bart Keijser, TNO,
André Boorsma, TNO

#### Covid-19 test facility:
[InBiome](https://inbiome.com/)

#### Online sources:

[RIVM covid-19 richtlijnen](https://lci.rivm.nl/richtlijnen/covid-19)

[OECD Policy Responses to Coronavirus (COVID-19)](http://www.oecd.org/coronavirus/policy-responses/testing-for-covid-19-a-way-to-lift-confinement-restrictions-89756248/#section-d1e222)

[SNOMED CT en Covid-19](https://www.snomed.org/snomed-ct/covid-19)

[nictiz COVID-19-terminologie](https://www.nictiz.nl/standaardisatie/terminologiecentrum/covid-19-terminologie/)

[hl7 FHIR](https://www.hl7.org/fhir/)

[Bioportal CODO ontology](https://bioportal.bioontology.org/ontologies/CODO)

[RIVM covid-19 testen](https://www.rivm.nl/coronavirus-covid-19/testen)

[InBiome](https://inbiome.com/)

## References

de Almeida Falbo, R. (2014, September). SABiO: Systematic Approach for Building Ontologies. In ONTO. COM/ODISE@ FOIS.

Lohmann, S., Link, V., Marbach, E., & Negru, S. (2014, November). WebVOWL: Web-based visualization of ontologies. In International Conference on Knowledge Engineering and Knowledge Management (pp. 154-158). Springer, Cham.

Peroni, S. (2016). A simplified agile methodology for ontology development. In OWL: Experiences and Directions–Reasoner Evaluation (pp. 55-69). Springer, Cham.

Van Rees, R. (2003). Clarity in the usage of the terms ontology, taxonomy and classification. CIB REPORT, 284(432), 1-8.


# License 

COTI is released under Apache 2.0, for more information see LICENSE.
